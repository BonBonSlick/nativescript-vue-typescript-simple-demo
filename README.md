# NativeScript-Vue Application

- test demo [Docs](https://www.nativescript.org/blog/nativescript-vue-with-class-components)
- NativeScript install [Docs 1](https://docs.nativescript.org/start/ns-setup-linux)
- NativeScript install [Docs 2](https://docs.nativescript.org/start/quick-setup#quick-setup)


## Usage

``` bash
# Install dependencies
npm install

# Preview on device
tns preview

# Build, watch for changes and run the application
tns run

# Build, watch for changes and debug the application
tns debug <platform>

# Build for production
tns build <platform> --env.production

```
